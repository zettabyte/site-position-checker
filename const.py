class CDBQuery:
    pragmaFK = 'PRAGMA foreign_keys = ON'
    createTableCat = '''CREATE TABLE IF NOT EXISTS 'Category' ('id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,   
                                                                    'name' TEXT UNIQUE,
                                                                    'url' TEXT);'''
    createTbaleKey = '''CREATE TABLE IF NOT EXISTS 'Keyword' ('id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                                                  'value' TEXT NOT NULL,
                                                                  'category_id' INTEGER NOT NULL,
                                                                   UNIQUE(value, category_id),
                                                                   FOREIGN KEY('category_id') REFERENCES Category ( id ) ON DELETE CASCADE);'''
    createTableHist = '''CREATE TABLE IF NOT EXISTS 'History' ('id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                                                  'check_date' TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                                                  'position' INTEGER,
                                                                  'keyword_id' INTEGER, 
                                                                   FOREIGN KEY('keyword_id') REFERENCES Keyword ( id ) ON DELETE CASCADE);'''
    selectCatsName = '''SELECT * FROM Category ORDER BY Category.name'''
    selectKeys = '''SELECT Keyword.id, Keyword.value as keyword, '' as position, '' as url FROM Keyword WHERE Keyword.category_id = ? ORDER BY Keyword.value'''
    selectHistDate = '''SELECT DISTINCT datetime(History.check_date, "localtime") as real,  strftime('%d.%m.%Y\n%H:%M:%S',(datetime(History.check_date, "localtime"))) as show FROM Keyword INNER JOIN History ON Keyword.id = History.keyword_id WHERE Keyword.category_id = ? ORDER BY History.check_date DESC'''
    
    class selectHistPos:
        mainSelect = '''SELECT Keyword.id as id, Keyword.value as keyword {select} FROM Keyword {joinTables} WHERE Keyword.category_id = ? ORDER BY Keyword.value'''
        subSelect = '''LEFT OUTER JOIN (SELECT History.position, History.keyword_id FROM History WHERE datetime(History.check_date, "localtime") = "{header}") AS {tableName} ON {tableName}.keyword_id = Keyword.id '''

    insertCat = '''INSERT INTO Category (name, url) VALUES  (?, ?)'''
    insertKey = '''INSERT INTO Keyword (value, category_id) VALUES  (?, ?)'''
    updateCat = '''UPDATE Category SET name = ?, url = ? WHERE name = ?'''
    delCat = '''DELETE FROM Category WHERE name = ?'''
    delKey = '''DELETE FROM Keyword WHERE id IN ({0})'''
    insertHist = '''INSERT INTO History (keyword_id, position) VALUES (?,?)'''
    delHistDate = '''DELETE FROM History WHERE datetime(History.check_date, "localtime") = ?'''

#=================================================================

class CSettings:
    confFileName = 'config.ini'
    searchDepth = 50
    autoSave = True
    lastPath = ''
    googleDomain = 'com.ua'
    googleUrl = 'http://google.'
    projectsFolder = 'projects/'
    projectsExt = 'spcd'
    exportExt = 'csv'
    
#=================================================================
    
class CKeysModel():
    headers = ['Keyword', 'Position', 'URL']
    searchParams = '/search?'
    urlQueryVarName = 'q'
    urlPageVarName = 'num'
    htmlSelectors = '#search h3.r a[href^="/"]'
    urlName = 'href'
    urlParam = '/url?q'

#=================================================================

class CDlgMsg():
    msg001 = "File path does not exist or is invalid:\n{0}"
    msg002 = "Category with name \n'{0}'\n already exists. \nPlease, choose another name."
    msg003 = "Please, fill in all fields."
    msg004 = "Are you sure want to delete category\n'{0}'? " 
    msg005 = "Some of keywords already exist in the list. \nEvery keyword in category must be unique."
    msg006 = "Are you sure want to delete keywords\n'{0}'?"
    msg007 = "Please, select one or more keywords to delete."
    msg008 = "Checking was stopped due to network error! \n\nPlease, check your network and application setting. \nError message is:\n\n"
    msg009 = "Checking was stopped!"
    msg010 = "Data successfully exported."
    msg011 = "Please, select column to delete."
    msg012 = "Are you sure want to delete date\n'{0}'?"
    msg013 = "Task accomplished!"
    msg014 = "Data was successfully exported!"
    
#=================================================================
    
class CDlgHdr:
    hdr001 = "Open project"
    hdr002 = "Information"
    hdr003 = "Save Project"
    hdr004 = "Warning"
    hdr005 = "Delete category"
    hdr006 = "Delete keyword(s)"
    hdr007 = "Export data"
    hdr008 = "Delete date"
    hdr009 = "Create category"
    hdr010 = "Update category"
    hdr011 = "Add keywords"
    hdr012 = "Site Positions Checker"
    hdr013 = "Insert CAPTCHA"
    hdr014 = "Settings"
    hdr015 = "About"
    
#=================================================================

if __name__ == '__main__': 
    pass