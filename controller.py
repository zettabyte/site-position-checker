from PyQt4.QtGui import QMessageBox, QItemSelectionModel, QFileDialog
from PyQt4.QtCore import Qt, pyqtSignal, QObject
import sqlite3, os, csv
from ui.view import MainWindow, CrudDlg, CaptchDlg, SettingsDlg, AboutDlg
from model import CategoryModel, KeywordsModel, HistoryModel, AppSettings, DataBase
from const import CSettings, CDlgMsg, CDlgHdr

#=================================================================
        
class ProjectController(QObject):
    projectOpened = pyqtSignal()
    
    def __init__(self, mainWin):
        QObject.__init__(self)
        self.mainWin = mainWin
        self.db = DataBase()
    
    
    def projectOpen(self, path = None):
        if not path:
            path = QFileDialog.getOpenFileName(self.mainWin, CDlgHdr.hdr001, directory = CSettings.projectsFolder, filter = '*.' + CSettings.projectsExt)
        if path:
            if os.path.exists(path):
                self.db.connectDatabase(path)
                CSettings.lastPath = path
                self._setupMainWin(path)
                self.projectOpened.emit()
            else:
                QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg001.format(path))
    
    
    def projectCreate(self): 
        path = QFileDialog.getSaveFileName(self.mainWin, CDlgHdr.hdr003, directory = CSettings.projectsFolder, filter = '*.' + CSettings.projectsExt)
        if path:
            self.db.connectDatabase(path)
            self.projectOpen(path)


    def _setupMainWin(self, path):
        self.mainWin.setWindowTitle(CDlgHdr.hdr012 + ' - ' + path)
        self.mainWin.actionNewCat.setEnabled(True)
        self.mainWin.groupBoxCat.setEnabled(True)

#=================================================================
        
class CategoryController(QObject):
    
    def __init__(self, mainWin):
        QObject.__init__(self)
        self.mainWin = mainWin
        self.model = CategoryModel()
        self.mainWin.listCategory.setModel(self.model)
        self.selection = self.mainWin.listCategory.selectionModel()


    def getSelectedCatUrl(self):
        return self.model.rows[self.selection.currentIndex().row()]['url']

    
    def getSelectedCatId(self):
        return self.model.rows[self.selection.currentIndex().row()]['id']

    
    def loadData(self, catIndex = 0):
        self.model.loadData()

        self.mainWin.actionEditCat.setEnabled(False)
        self.mainWin.actionDelCat.setEnabled(False)
        self.mainWin.tabWidget.setEnabled(False)
        self.mainWin.siteUrlGroupBox.setEnabled(False)
        self.mainWin.siteUrlLabel.setText('')

        if self.setCatSelection(catIndex) >= 0:
            self.mainWin.actionEditCat.setEnabled(True)
            self.mainWin.actionDelCat.setEnabled(True)
            self.mainWin.tabWidget.setEnabled(True)
            self.mainWin.siteUrlGroupBox.setEnabled(True)


    def setCatSelection(self, catIndex): 
        index = self.model.index(catIndex, 0)
        self.selection.setCurrentIndex(index , QItemSelectionModel.SelectCurrent)
        return index.row()    


    def newCatDlgOpen(self):
        self.crudDlg = CrudDlg().createCategory()
        self.crudDlg.buttonBox.accepted.connect(self._createCat)
        self.crudDlg.exec_()

        
    def _createCat(self):
        name = self.crudDlg.lineEditCatName.text().strip()
        siteUrl = self.crudDlg.lineEditUrl.text().strip()
#         Delete all empty rows + delete spaces from begin and end of every row + erase all doubles rows
        keywordsText = self.crudDlg.textEditKeywords.toPlainText().strip()
        keywordsList = []
        if keywordsText:
            keywordsList = list(filter((lambda i: i != ''), set(map(str.strip, keywordsText.split('\n')))))
        if name and siteUrl:
            try:
                newCatId = self.model.createCat(name, siteUrl, keywordsList)
                self.loadData()
                self.setCatSelection([i['id'] for i in self.model.rows].index(newCatId))
                self.crudDlg.accept()
            except sqlite3.IntegrityError as e:
                if 'UNIQUE constraint failed' in str(e):
                    QMessageBox.warning(self.crudDlg, CDlgHdr.hdr004, CDlgMsg.msg002.format(name))
                    self.crudDlg.lineEditCatName.setFocus()
        else:
            QMessageBox.warning(self.crudDlg, CDlgHdr.hdr004, CDlgMsg.msg003)


    def updateCatDlgOpen(self):
        self.crudDlg = CrudDlg().updateCategory()
        self.crudDlg.lineEditCatName.setText(self.selection.currentIndex().data())
        self.crudDlg.lineEditUrl.setText(self.getSelectedCatUrl())
        self.crudDlg.buttonBox.accepted.connect(self._updateCat)
        self.crudDlg.exec_()


    def _updateCat(self): 
        newName = self.crudDlg.lineEditCatName.text().strip()
        newUrl = self.crudDlg.lineEditUrl.text().strip()
        if newName and newUrl:
            try:
                self.model.updateCat(self.selection.currentIndex().data(), newName, newUrl)
                self.loadData()
                self.setCatSelection([i['name'] for i in self.model.rows].index(newName))
                self.crudDlg.accept()
            except sqlite3.IntegrityError as e:
                if 'UNIQUE constraint failed' in str(e):
                    QMessageBox.warning(self.crudDlg, CDlgHdr.hdr004, CDlgMsg.msg002.format(newName))
                    self.crudDlg.lineEditCatName.setFocus()
        else:
            QMessageBox.warning(self, CDlgHdr.hdr004, CDlgMsg.msg003)


    def delCat(self):
        catName = self.selection.currentIndex().data()
        res = QMessageBox.question(self.mainWin, CDlgHdr.hdr005, CDlgMsg.msg004.format(catName), buttons=QMessageBox.Ok | QMessageBox.Cancel, defaultButton=QMessageBox.Cancel)
        if res == QMessageBox.Ok:
            self.model.delCat(catName)
            self.loadData()

#=================================================================
            
class KeywordsController(QObject):
    keywordsListChanged = pyqtSignal()
    
    def __init__(self, mainWin):
        QObject.__init__(self)
        self.mainWin = mainWin
        
        self.model = KeywordsModel()
        self.mainWin.tableKeywords.setModel(self.model)
        self.selection = self.mainWin.tableKeywords.selectionModel()
        self.model.keywordChecked.connect(self.keywordChecked)
        self.captchaDlg = CaptchDlg(self.model.webPage)
        self.captchaDlg.rejected.connect(self.stopCkecking)
        self.model.captchaShow.connect(self.captchaDlg.show)
        self.model.captchaHide.connect(self.captchaDlg.hide)
        self.model.errorOccurred.connect(self.stopCkecking)
    

    def keywordChecked(self):
        self.mainWin.progressBar.setValue(self.mainWin.progressBar.value() + 1)
        self.selection.setCurrentIndex(self.model.index(self.mainWin.progressBar.value(), 0), QItemSelectionModel.Columns)

    
    def loadData(self, **newCategory):
        if newCategory:
            self.currentCategory = newCategory
        self.model.loadData(**self.currentCategory)
        self.model.modelReset.emit()
        keysCount = len(self.model.rows)
        self.mainWin.widgetCheck.setEnabled(False)
        if keysCount > 0:
            self.mainWin.progressBar.setMaximum(keysCount)
            self.mainWin.widgetCheck.setEnabled(True)
            
            self.mainWin.tableKeywords.resizeColumnsToContents()
            self.mainWin.tableKeywords.horizontalHeader().stretchLastSection()

    
    def addKeysDlgOpen(self):
        self.crudDlg = CrudDlg().addKeywords()
        self.crudDlg.buttonBox.accepted.connect(self._addKeys)
        self.crudDlg.exec_()

    
    def _addKeys(self):
        keywordsText = self.crudDlg.textEditKeywords.toPlainText().strip() 
        keywordsList = []
        if keywordsText: 
#             Create list + delete spaces from begin and end of every row + erase all doubles rows
            keywordsList = list(filter((lambda i: i != ''), set(map(str.strip, keywordsText.split('\n')))))
        try:
            self.model.addKeywords(keywordsList)
            self.loadData()
            self.keywordsListChanged.emit()
            self.crudDlg.accept()
        except sqlite3.IntegrityError as e:
            if 'UNIQUE constraint failed' in str(e):
                QMessageBox.warning(self.crudDlg, CDlgHdr.hdr004, CDlgMsg.msg005)
                self.crudDlg.textEditKeywords.setFocus()


    def delKeywords(self):
        keyIdList = [self.model.rows[i.row()][0] for i in self.selection.selectedRows()]
        keyNameList = [self.model.rows[i.row()][1] for i in self.selection.selectedRows()]
        if  keyIdList:
            res = QMessageBox.question(self.mainWin, CDlgHdr.hdr006, CDlgMsg.msg006.format(', '.join(keyNameList)), buttons=QMessageBox.Ok | QMessageBox.Cancel, defaultButton=QMessageBox.Cancel)
            if res == QMessageBox.Ok:
                self.model.delKeywords(keyIdList)
                self.loadData()
                self.keywordsListChanged.emit()
        else:
            QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg007)


    def uiCheckingEnable(self, setEnable = True):
        self.mainWin.buttonStop.setEnabled(not setEnable)        
        self.mainWin.buttonCheck.setEnabled(setEnable)

        self.mainWin.buttonCheck.setEnabled(setEnable)
        self.mainWin.groupBoxCat.setEnabled(setEnable)
        self.mainWin.widgetKeys.setEnabled(setEnable)
        self.mainWin.buttonExpKeys.setEnabled(setEnable)
        self.mainWin.actionNewCat.setEnabled(setEnable)
        self.mainWin.actionEditCat.setEnabled(setEnable)
        self.mainWin.actionDelCat.setEnabled(setEnable)
        self.mainWin.actionOpenProject.setEnabled(setEnable)
        self.mainWin.actionNewProject.setEnabled(setEnable)
        self.mainWin.actionOpenLastProject.setEnabled(setEnable)
        self.mainWin.actionSettings.setEnabled(setEnable)


    def checkPositions(self):
        self.uiCheckingEnable(False)
        self.model.checkPositions()

    
    def stopCkecking(self, errorMsg = ''):
        self.model.fRunning = False
        self.loadData()
        self.mainWin.progressBar.setValue(0)
        self.uiCheckingEnable(True)
        
        if errorMsg:
            QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg008 + errorMsg)
        else:
            QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg009)
    

    def csvExport(self):
        path = QFileDialog.getSaveFileName(self.mainWin, CDlgHdr.hdr007, directory = CSettings.projectsFolder, filter = '*.' + CSettings.projectsExt)
        if path:
            res =  [self.model.headers] + [''] + [i[1:] for i in self.model.rows]
            with open(path, 'w', newline='\n', encoding='utf-8') as csvfile:
                csvWriter = csv.writer(csvfile)
                csvWriter.writerows(res)
                QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg010)

    
#=================================================================

class HistoryController(QObject):

    def __init__(self, mainWin):
        QObject.__init__(self)
        self.model = HistoryModel()
        self.mainWin = mainWin
        
        self.mainWin.tableHistory.setModel(self.model)
        self.selection = self.mainWin.tableHistory.selectionModel()

        
    def loadData(self, newCatId = None):
        if newCatId:
            self.catId = newCatId
        self.model.loadData(self.catId)
        self.mainWin.tableHistory.resizeColumnsToContents()


    def addToHistory(self, keyPositionList):
        if CSettings.autoSave:
            self.model.addToHistory(keyPositionList)

    
    def delDate(self):
        colIndex = self.selection.currentIndex().column()
        if colIndex >= 0: 
            date = self.model.headers[colIndex]['show'].replace('\n', ' ')
            res = QMessageBox.question(self.mainWin, CDlgHdr.hdr008, CDlgMsg.msg012.format(date), buttons=QMessageBox.Ok | QMessageBox.Cancel, defaultButton=QMessageBox.Cancel)
            if res == QMessageBox.Ok:
                self.model.delDate(self.model.headers[colIndex]['real'])
                self.loadData()
        else:
            QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg011)

#=================================================================

class AppSettingController(QObject):
    
    def __init__(self):
        QObject.__init__(self)
        self.settings = AppSettings()

    
    def showSettingsDlg(self):
        self.dlg = SettingsDlg()

        self.dlg.buttonBox.accepted.connect(self.setSettings)
        self.dlg.buttonBox.rejected.connect(self.dlg.reject)

        self.dlg.googleDomain.setText(CSettings.googleDomain)
        self.dlg.depth.setValue(CSettings.searchDepth)
        self.dlg.autosave.setCheckState(Qt.Checked) if CSettings.autoSave else self.dlg.autosave.setCheckState(Qt.Unchecked)

        self.dlg.adjustSize()
        self.dlg.setFocus()
        self.dlg.exec_()

    
    def setSettings(self):
        CSettings.autoSave = True if self.dlg.autosave.checkState() == Qt.Checked else False
        CSettings.searchDepth = self.dlg.depth.value()
        CSettings.googleDomain = self.dlg.googleDomain.text()
        self.dlg.accept()

#=================================================================

class MainController(QObject):
    
    def __init__(self):
        QObject.__init__(self)
#         Setup aplication
        self.mainWin = MainWindow()
        self.settings = AppSettings()
        self.settings.loadConf()
#         Init about dialog. It is very important feature :)
        self.about = AboutDlg()        
#         Create all controllers
        self.project = ProjectController(self.mainWin)
        self.category = CategoryController(self.mainWin)
        self.keywords = KeywordsController(self.mainWin)
        self.history = HistoryController(self.mainWin)
        self.appSettings = AppSettingController()
#         Init path to last opened project
        if CSettings.lastPath:
            self.mainWin.actionOpenLastProject.setVisible(True)
            self.mainWin.actionOpenLastProject.setText('Open: ' + CSettings.lastPath)
            self.mainWin.actionOpenLastProject.triggered.connect(lambda: self.project.projectOpen(CSettings.lastPath))
#         Connect UI signals to slots
        self.mainWin.actionExit.triggered.connect(self.mainWin.close)
        self.mainWin.actionNewProject.triggered.connect(self.project.projectCreate)
        self.mainWin.actionOpenProject.triggered.connect(self.project.projectOpen)
        self.mainWin.actionNewCat.triggered.connect(self.category.newCatDlgOpen)
        self.mainWin.actionEditCat.triggered.connect(self.category.updateCatDlgOpen)
        self.mainWin.actionDelCat.triggered.connect(self.category.delCat)
        self.mainWin.actionSettings.triggered.connect(self.showSettings)
        self.mainWin.actionAbout.triggered.connect(self.about.show)
        self.mainWin.buttonAddKeys.clicked.connect(self.keywords.addKeysDlgOpen)
        self.mainWin.buttonDelKeys.clicked.connect(self.keywords.delKeywords)
        self.mainWin.buttonCheck.clicked.connect(self.keywords.checkPositions)
        self.mainWin.buttonStop.clicked.connect(self.keywords.stopCkecking)
        self.mainWin.buttonExpKeys.clicked.connect(lambda: self.exportTableToCSV(self.keywords.model))
        self.mainWin.buttonExpHistory.clicked.connect(lambda: self.exportTableToCSV(self.history.model))
        self.mainWin.deleteDate.clicked.connect(self.history.delDate)
#         Connect signals slots form app model
        self.project.projectOpened.connect(self.projectOpened)
        self.category.selection.currentRowChanged.connect(self.categoryChanged)
        self.keywords.model.chackingFinished.connect(self.checkingFinished)
        self.keywords.keywordsListChanged.connect(self.history.loadData)
        self.category.model.modelReset.connect(self.removeLastCat)
#         Other staff
        self.mainWin.labelGoogle.setText(CSettings.googleUrl + CSettings.googleDomain)

    def projectOpened(self):
        self.settings.writeConf()
        self.category.loadData()
    
    def showSettings(self):
        self.appSettings.showSettingsDlg()
        if self.appSettings.dlg.result():
            self.settings.writeConf()
            self.mainWin.labelGoogle.setText(CSettings.googleUrl + CSettings.googleDomain)
    
    def removeLastCat(self):
        if self.category.model.rowCount(None) == 0:
            self.keywords.model.clearData()
            self.history.model.clearData()

    def checkingFinished(self):
        QMessageBox.information(self.mainWin, CDlgHdr.hdr002, CDlgMsg.msg013)
        self.mainWin.progressBar.setValue(0)
        self.keywords.uiCheckingEnable(True)
        
        keyPositionList = [[i[0], i[2]] for i in self.keywords.model.rows]
        self.history.addToHistory(keyPositionList)
        self.history.loadData(self.category.getSelectedCatId())

    
    def categoryChanged(self, newModIdex, prevModIndex):
        siteUrl = self.category.getSelectedCatUrl()
        catId = self.category.getSelectedCatId()
        urlHtml = '<a href="{0}">{0}</a>'
        self.mainWin.siteUrlLabel.setText(urlHtml.format(siteUrl))
        self.keywords.loadData(catId = catId, url = siteUrl)
        self.history.loadData(catId)


    def exportTableToCSV(self, model):
        path = QFileDialog.getSaveFileName(self.mainWin, CDlgHdr.hdr007, directory = CSettings.projectsFolder, filter = '*.' + CSettings.exportExt)
        if path:
            res = [['']]
            #Make header of table
            for i in range(model.columnCount(None)):
                res[0].append(model.headerData(i, Qt.Horizontal, Qt.DisplayRole))
            res.append('')
            #Make rows of table - vertical header + item data
            for i in range(model.rowCount(None)):
                row = [model.headerData(i, Qt.Vertical, Qt.DisplayRole)]
                for j in range(model.columnCount(None)):
                    row.append(model.index(i, j).data())
                res.append(row)
                
            with open(path, 'w', newline='\n', encoding='cp1251') as csvfile:
                csvWriter = csv.writer(csvfile, delimiter =';', quoting=csv.QUOTE_ALL)
                csvWriter.writerows(res)
                QMessageBox.information(self.mainWin, CDlgHdr.hdr007, CDlgMsg.msg014)


#=================================================================

if __name__ == '__main__': 
    pass