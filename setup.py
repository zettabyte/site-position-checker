import sys
from cx_Freeze import setup, Executable


includes = []
include_files = ['icons/']
excludes = ['tkinter']
packages = ['os']
path = []

base = None
if sys.platform == "win32":
    base = "Win32GUI"
 
setup(  name = "SitePositionChecker",
        version = "0.1",
        description = "Check site positions in Google",
        options = {"build_exe": {'build_exe': 'SitePositionChecker',
                                 "include_msvcr": True,
                                 "includes": includes,
                                 "excludes": excludes,
                                 "packages": packages,
                                 'include_files': include_files,
                                 "path": path}},        
                   executables = [Executable("main.py",
                                  icon="icons/logo.ico",
                                  targetName = 'spchecker.exe',
                                  base=base)])