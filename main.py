from controller import MainController
import sys
from PyQt4.QtGui import QApplication

if __name__ == '__main__':
    app = QApplication(sys.argv)

    controller = MainController()    
    sys.exit(app.exec_())        