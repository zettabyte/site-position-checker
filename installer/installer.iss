; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{9DE2CCC4-2C7F-412E-897B-FC10B24B286E}
AppName=Site Position Checker
AppVersion=0.1
;AppVerName=Site Position Checker 0.1
AppPublisher=Zettabyte, Inc.
AppPublisherURL=https://bitbucket.org/zettabyte/sitepositionchecker
AppSupportURL=https://bitbucket.org/zettabyte/sitepositionchecker
AppUpdatesURL=https://bitbucket.org/zettabyte/sitepositionchecker
DefaultDirName={pf}\SPChecker
DefaultGroupName=Site Position Checker
OutputDir=C:\Home\Python\workspace\spchecker\installer
OutputBaseFilename=spchecker_setup
SetupIconFile=C:\Home\Python\workspace\spchecker\SitePositionChecker\icons\logo.ico
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Home\Python\workspace\spchecker\SitePositionChecker\spchecker.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Home\Python\workspace\spchecker\SitePositionChecker\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\Site Position Checker"; Filename: "{app}\spchecker.exe"
Name: "{group}\{cm:UninstallProgram,Site Position Checker}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\Site Position Checker"; Filename: "{app}\spchecker.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\spchecker.exe"; Description: "{cm:LaunchProgram,Site Position Checker}"; Flags: nowait postinstall skipifsilent

