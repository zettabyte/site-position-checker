from PyQt4 import uic
from PyQt4.QtGui import QMainWindow, QApplication, QDialog, QIcon
from const import CDlgHdr
from ui.settings_ui import Ui_DialogSettings
from ui.mainwindow_ui import Ui_MainWindow
from ui.captcha_ui import Ui_DialogCaptcha
from ui.crud_ui import Ui_DialogCrud
from ui.about_ui import Ui_DialogAbout

class MainWindow (QMainWindow, Ui_MainWindow):
    
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        
        self.setWindowIcon(QIcon('icons/window-icon.png'))
        self.actionSettings.setIcon(QIcon('icons/settings.png'))
        self.actionOpenProject.setIcon(QIcon('icons/open.png'))
        self.actionNewProject.setIcon(QIcon('icons/new.png'))
        self.buttonCheck.setIcon(QIcon('icons/check.png'))
        self.buttonDelKeys.setIcon(QIcon('icons/remove-key.png'))
        self.buttonAddKeys.setIcon(QIcon('icons/add-key.png'))
        self.deleteDate.setIcon(QIcon('icons/remove-date.png'))
        self.actionNewCat.setIcon(QIcon('icons/add-cat.png'))
        self.actionEditCat.setIcon(QIcon('icons/edit-cat.png'))
        self.actionDelCat.setIcon(QIcon('icons/del-cat.png'))
        self.actionAbout.setIcon(QIcon('icons/about.png'))
        self.actionOpenLastProject.setIcon(QIcon('icons/reopen.png'))
        self.buttonStop.setIcon(QIcon('icons/stop.png'))
        self.buttonExpKeys.setIcon(QIcon('icons/export.png'))
        self.buttonExpHistory.setIcon(QIcon('icons/export.png'))

        self.actionNewCat.setEnabled(False)
        self.groupBoxCat.setEnabled(False)
        self.actionEditCat.setEnabled(False)
        self.actionDelCat.setEnabled(False)
        self.siteUrlGroupBox.setEnabled(False)
        self.tabWidget.setEnabled(False)
        self.actionEditCat.setEnabled(False)
        self.actionDelCat.setEnabled(False)
        self.buttonStop.setEnabled(False)
        self.actionOpenLastProject.setVisible(False)
        
        self.setWindowTitle(CDlgHdr.hdr012)
            
        self.show()
     
#=================================================================

class CaptchDlg(QDialog, Ui_DialogCaptcha):
    
    def __init__(self, page):
        QDialog.__init__(self)
        self.setupUi(self)
        self.webView.setPage(page)
        self.setWindowTitle(CDlgHdr.hdr013)
        self.setWindowIcon(QIcon('icons/captcha.png'))        
    
    
    def show(self):
        QApplication.beep()
        QDialog.show(self)
    
#=================================================================

class SettingsDlg(QDialog, Ui_DialogSettings):
    
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setWindowTitle(CDlgHdr.hdr014)
        self.setWindowIcon(QIcon('icons/settings.png'))
        
#=================================================================

class CrudDlg(QDialog, Ui_DialogCrud):
    @classmethod
    def createCategory(cls):
        obj = cls()
        obj.setWindowTitle(CDlgHdr.hdr009)
        obj.setWindowIcon(QIcon('icons/add-cat.png'))
        obj.buttonBox.rejected.connect(obj.reject)
        obj.adjustSize()
        obj.setFocus()
        return obj
    
    @classmethod
    def updateCategory(cls):
        obj = cls()
        obj.setWindowTitle(CDlgHdr.hdr010)
        obj.setWindowIcon(QIcon('icons/edit-cat.png'))
        obj.textEditKeywords.setVisible(False)
        obj.labelKeywords.setVisible(False)
        obj.buttonBox.rejected.connect(obj.reject)
        obj.adjustSize()
        obj.lineEditCatName.setFocus()
        return obj
    
    @classmethod
    def addKeywords(cls):
        obj = cls()
        obj.setWindowTitle(CDlgHdr.hdr011)
        obj.setWindowIcon(QIcon('icons/add-key.png'))
        obj.labelCatName.setVisible(False)
        obj.lineEditCatName.setVisible(False)
        obj.labelSiteUrl.setVisible(False)
        obj.lineEditUrl.setVisible(False)
        obj.buttonBox.rejected.connect(obj.reject)
        obj.adjustSize()
        obj.textEditKeywords.setFocus()
        return obj
    
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

#=================================================================

class AboutDlg(QDialog, Ui_DialogAbout):
    
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        self.buttonBox.clicked.connect(self.accept)
        self.setWindowTitle(CDlgHdr.hdr015)
        self.setWindowIcon(QIcon('icons/about.png'))
    
    def show(self):
        QDialog.show(self)
