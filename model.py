from PyQt4.QtCore import QAbstractTableModel, Qt, QUrl, QAbstractListModel, pyqtSignal
import urllib.parse, sqlite3, configparser, os
from const import CKeysModel, CDBQuery, CSettings
from PyQt4.Qt import QNetworkRequest, QWebPage

#=================================================================

# Singleton
class DataBase():
    _instance = None

    def __new__(self):
        if not self._instance:
            self._instance = super(DataBase, self).__new__(self)
        return self._instance
    
    
    def connectDatabase(self, path):
#         If file with database is not exist then database will be created
        self.connect = sqlite3.connect(path)
        self.connect.row_factory = sqlite3.Row
        self.cursor = self.connect.cursor()
        self.cursor.execute(CDBQuery.pragmaFK)    
        select = [CDBQuery.createTableCat, CDBQuery.createTbaleKey, CDBQuery.createTableHist]
        with self.connect:
            for i in select: self.cursor.execute(i)

#=================================================================    

class HistoryModel(QAbstractTableModel):
    
    def __init__(self):
        QAbstractTableModel.__init__(self)
        self._db = DataBase()
        
        self.headers = []
        self.rows = []

    
    def rowCount(self, parent):
        return len(self.rows)

    
    def columnCount(self, parent):
        return len(self.headers)


    def data(self, index, role):
        if role == Qt.DisplayRole and self.rows:
            return self.rows[index.row()][index.column() + 2]

    
    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.headers[section]['show']
            if orientation == Qt.Vertical:
                return self.rows[section]['keyword']


    def _createQuery(self):
#         Create multiple select statements
        position = ''
        subSelect = ''
        for i in range(len(self.headers)):
            subTableName = 'Table' + str(i)
            position += ', ' + subTableName + '.position'
            subSelect += CDBQuery.selectHistPos.subSelect.format(header = self.headers[i]['real'], tableName = subTableName)
            
        select = CDBQuery.selectHistPos.mainSelect.format(select = position, joinTables = subSelect)
        return select


    def loadData(self, catId):
        self.catId = catId

        self.beginResetModel()
        self.headers = list(self._db.cursor.execute(CDBQuery.selectHistDate, (self.catId,)))
        select = self._createQuery()
        self.rows = list(self._db.cursor.execute(select, (self.catId,)))
        self.endResetModel()

        
    def clearData(self):
        self.beginResetModel()
        self.headers = []
        self.rows = []
        self.endResetModel()

    
    def addToHistory(self, positionList):
        with self._db.connect:
            self._db.cursor.executemany(CDBQuery.insertHist, positionList)

   
    def delDate(self, checkDate):
        with self._db.connect:
            self._db.cursor.execute(CDBQuery.delHistDate, (checkDate,))        
    
#=================================================================        

class KeywordsModel(QAbstractTableModel):
    chackingFinished = pyqtSignal()
    captchaHide = pyqtSignal()
    captchaShow = pyqtSignal()
    keywordChecked = pyqtSignal()
    errorOccurred = pyqtSignal(str)

    def __init__(self):
        QAbstractTableModel.__init__(self)
        self._db = DataBase()
        
        self._fCaptchaShow = False
        self.fRunning = False
        self.cursor = 0
        self.catId = None
        
        self.webPage = QWebPage()
        self.webPage.networkAccessManager().finished.connect(self._handleHttpStatus)

        self.rows = []
        self.headers = []


    def rowCount(self, parent):
        return len(self.rows)

    
    def columnCount(self, parent):
        return len(self.headers)


    def data(self, index, role):
        if role == Qt.DisplayRole and self.rows:
            return self.rows[index.row()][index.column() + 1]

    
    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return CKeysModel.headers[section]
            if orientation == Qt.Vertical:
                return section + 1


    def loadData(self, **category):
        self.catId = category['catId']
        self.siteUrl = category['url']
        self.cursor = 0
        self.beginResetModel()
        self.headers = CKeysModel.headers
        self.rows = [list(i) for i in self._db.cursor.execute(CDBQuery.selectKeys, (self.catId,))]
        self.endResetModel()


    def clearData(self):
        self.beginResetModel()
        self.headers = []
        self.rows = []
        self.endResetModel()


    def addKeywords(self, keywordsList):
        params = [(i, self.catId) for i in keywordsList]
        with self._db.connect:
            self._db.cursor.executemany(CDBQuery.insertKey, params)

    
    def delKeywords(self, keyIdList):
#         Select needs to have '?' chars such times as number of keywords in list
        select = CDBQuery.delKey.format(','.join('?' * len(keyIdList)))
        with self._db.connect:
            self._db.cursor.execute(select, keyIdList)


    def checkPositions(self):
        self.cursor = 0
        self.fRunning = True
        self._loadUrl()


    def _loadUrl(self):
        self.beginResetModel()
        self.rows[self.cursor][2] = ''
        self.rows[self.cursor][3] = ''
        self.endResetModel()
        
        keyword = self.rows[self.cursor][1]
        urlvars = urllib.parse.urlencode({CKeysModel.urlQueryVarName: keyword, CKeysModel.urlPageVarName: CSettings.searchDepth})
        url = QUrl.fromEncoded(CSettings.googleUrl+CSettings.googleDomain + CKeysModel.searchParams + urlvars)
        self.webPage.mainFrame().load(url)
        

    def _handleHttpStatus(self, networkReply):
        statCod = networkReply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        if self.fRunning and statCod:
            if statCod == 204: 
                if self._fCaptchaShow:
                    self._fCaptchaShow = False
                    self.captchaHide.emit()
                self._parseHtml()
                if self.cursor < len(self.rows) - 1:
                    self.cursor += 1
                    self._loadUrl()
                else:
                    self.fRunning = False
                    self.cursor = 0
                    self.chackingFinished.emit()
            elif statCod == 503:
                self._fCaptchaShow = True
                self.captchaShow.emit()
            elif statCod not in range(200, 400):
                self.errorOccurred.emit(networkReply.errorString())


    def _parseHtml(self): 
        snippets = self.webPage.mainFrame().documentElement().findAll(CKeysModel.htmlSelectors)
        for i in range(len(snippets)):
            href = snippets[i].attribute(CKeysModel.urlName)
            if not CKeysModel.urlParam in href:
                continue
            url = ''.join(urllib.parse.parse_qs(href)[CKeysModel.urlParam])
            if self.siteUrl in url:
                self.beginResetModel()
                self.rows[self.cursor][2] = str(i + 1)
                self.rows[self.cursor][3] = url
                self.endResetModel()
                break
        else:
            self.beginResetModel()
            self.rows[self.cursor][2] = '-'
            self.rows[self.cursor][3] = '-'
            self.endResetModel()
        self.keywordChecked.emit()

#================================================================= 

class CategoryModel(QAbstractListModel):

    def __init__(self):
        QAbstractListModel.__init__(self)
        self._db = DataBase()
        self.rows = []


    def rowCount(self, parent):
        return len(self.rows)


    def data(self, index, role):
        if role == Qt.DisplayRole and self.rows:
            return self.rows[index.row()]['name']


    def loadData(self):
        self.beginResetModel()
        self.rows = list(self._db.cursor.execute(CDBQuery.selectCatsName))
        self.endResetModel()


    def createCat(self, name, siteUrl, keywordsList):
        with self._db.connect:
            self._db.cursor.execute(CDBQuery.insertCat, (name, siteUrl))
            newCatId = self._db.cursor.lastrowid
            for i in keywordsList:
                self._db.cursor.execute(CDBQuery.insertKey, (i, newCatId))
        return newCatId


    def updateCat(self, currentName, newName, newUrl):
        with self._db.connect:
            self._db.cursor.execute(CDBQuery.updateCat, (newName, newUrl, currentName))


    def delCat(self, name):
        with self._db.connect:
#             FOREIGN KEY ON DELETE CASCADE to del all related keywords and history
            self._db.cursor.execute(CDBQuery.delCat, (name,))
    
    
#=================================================================

# Singleton
class AppSettings():
    _instance = None

    def __new__(self):
        if not self._instance:
            self._instance = super(AppSettings, self).__new__(self)
        return self._instance


    def __init__(self):
        self.config = configparser.ConfigParser()
#         Create default folder for project files
        if not os.path.exists(CSettings.projectsFolder):
            os.makedirs(CSettings.projectsFolder)

    
    def loadConf(self):
        try:
            self.config.read(CSettings.confFileName)
            CSettings.searchDepth = self.config['Settings'].getint('depth')
            CSettings.autoSave = self.config['Settings'].getboolean('autosave')
            CSettings.lastPath = self.config['Settings'].get('path')
            CSettings.googleDomain = self.config['Settings'].get('domain')
        except:
            self.writeConf()
    
            
    def writeConf(self):
        try:
            self.config.add_section('Settings')
        except:
            pass
        self.config['Settings']['depth'] = str(CSettings.searchDepth)
        self.config['Settings']['autosave'] = str(CSettings.autoSave)
        self.config['Settings']['path'] = str(CSettings.lastPath)
        self.config['Settings']['domain'] = str(CSettings.googleDomain)
        with open(CSettings.confFileName, 'w') as cfile:
            self.config.write(cfile)

#=================================================================

if __name__ == '__main__': 
    pass